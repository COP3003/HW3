/*
 * FileName: Painter.java
 * Author: 5153
 * Created: 13 November, 2017
 * Last Modified: 17 November, 2017
 *
 * Included class:
 * <li>
 *   <ol>LayerList</ol>
 *   <ol>MyShape</ol>
 *   <ol>MyRectangle</ol>
 *   <ol>MyEllipse</ol>
 *   <ol>MyLine</ol>
 * </li>
 *
 * Painter creates, manipulates and tracks MyShape objects. <em>Note: these are
 *  not descended from java.awt.Shape.</em>
 */

import javax.swing.*;
import java.awt.*;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;
import java.util.LinkedList;

/**
 * Class Painter creates the painting surface object, tracks the selected
 *  paint options, and tracks the created MyShape objects.
 *
 * @author 5153
 */
class Painter extends JPanel {

  final LayerList<MyShape> layers;  // Each MyShape is 'painted' on a layer.
  Color fgColor;  // currently selected color
  Paint.PaintShape stencil; // currently selected shape
  boolean filled; // if the shape is to be filled when drawn
  MyShape selected; // used when moving a shape

  private Graphics2D g2;  // used to paint the shapes on the screen

  /**
   * Constructor. Sets the defaults for the paintable surface.
   *
   * @param defaultColor Color default color a shape is drawn in
   * @param defaultShape Paint.PaintShape default shape to draw
   */
  Painter(Color defaultColor, Paint.PaintShape defaultShape){
    setBackground(Color.white); // paint on a white surface
    filled = false; // toggles if a shape is painted filled or not
    layers = new LayerList<>(); // Custom Queue List that tracks the shapes
    fgColor = defaultColor; // currently selected paint color
    stencil = defaultShape; // currently select shape to paint
    selected = null;  // references a shape being moved
  }

  // redraws the shapes on this Painter surface
  @Override
  public void paintComponent(Graphics g){
    super.paintComponent(g);
    g2 = (Graphics2D)g.create(); // create a graphics object

    // Calls each shape's draw method starting with the first shape so they
    //  are painted(layered) in order of creation.
    layers.forEach(l->l.draw(g2));
    g2.dispose(); // cleanup the graphics object
  }

  /**
   * Based on the selected shape a new MyShape derived shape is create using
   *  the passed in position and currently selected values. The initial shape
   *  is only a point.
   * @param x1 int anchor X position of the shape
   * @param y1 int anchor Y position of the shape
   */
  void addLayer(int x1, int y1) {
    switch (stencil)
    {
      case RECTANGLE:
        layers.add(new MyRectangle(fgColor, filled, x1, y1, x1, y1));
        break;
      case OVAL:
        layers.add(new MyEllipse(fgColor, filled, x1, y1, x1, y1));
        break;
      case EDGE:
        layers.add(new MyLine(fgColor, x1, y1, x1, y1));
        break;
    }
  }

  /**
   * Removes the last shape created from the stack. If the stack is empty,
   *  nothing happens.
   */
  void undo(){
    if(!layers.isEmpty()) {
      layers.removeLast();
    }
  }

  /**
   * Removes all the shapes from the stack. No effect if there are no shapes.
   */
  void clear(){
    if(!layers.isEmpty()) {
      layers.clear();
    }
  }

  /**
   * Custom LinkList that stores MyShape derivatives as 'layers'.
   *
   * @param <E> MyShape generic type for any MyShape subclass
   */
  class LayerList<E extends MyShape> extends LinkedList<E>{

    // Default constructor. Implicitly calls LinkList's default constructor.
    LayerList(){
    }

    /**
     * Compare the position coordinate parameters against each shape starting
     *  from the first created. The first match is returned. If there is no
     *  match, null is returned. E is the specific MyShape type of that layer.
     *  N is an Integer generic limited to Integers or subclasses of.
     * @param x N X coordinate to compare against
     * @param y N Y coordinate to compare against
     * @return shape E null or the first matched shape object reference
     */
    <N extends Integer> E getShape(N x, N y){
      E shape = null;
      for (int i = size()-1; i >= 0; i--) {
        shape = (get(i).contains(x,y))?get(i):null;
        if (shape != null){
          break; // A match is made. Stop the search and return this reference.
        }
      }
      return shape;
    }
  }

  /**
   * Abstract Class representing what a MyShape shape needs to be. Defines the
   *  values: color, filled status, initial anchor point coordinates, and final
   *  coordinates.
   */
  abstract class MyShape {
    final Color color;
    final boolean filled;
    int x1;
    int y1;
    int x2;
    int y2;

    /**
     * Constructor. Initializes a shape's default characteristics.
     * @param color Color color of the shape
     * @param filled boolean if the shape is a solid
     * @param x1 int initial X anchor coordinate
     * @param y1 int initial Y anchor coordinate
     * @param x2 int initial X end coordinate
     * @param y2 int initial Y end coordinate
     */
    MyShape(Color color, boolean filled,
            int x1, int y1, int x2, int y2) {
      this.color = color;
      this.filled = filled;
      this.x1 = x1;
      this.y1 = y1;
      this.x2 = x2;
      this.y2 = y2;
    }

    /**
     * Abstract method that must be defined. Shape specific draw instructions.
     * @param g2 Graphics2D graphics object
     */
    abstract void draw(Graphics2D g2);

    /**
     * Abstract method that must be defined. Shape specific instructions for
     *  altering an existing shape's size and position.
     * @param x2 int new ending X coordinate
     * @param y2 int new ending Y coordinate
     */
    abstract void update(int x2, int y2);

    /**
     * Abstract method that must be defined. Shape specific instructions for
     *  checking if an (x,y) point is in the shape.
     * @param px int X position of the point to test
     * @param py int Y position of the point to test
     * @return boolean true if the point is in the shape
     */
    abstract boolean contains(int px, int py);

    /**
     * Abstract method that must be defined. Shape specific instructions for
     *  how to adjust a shapes location.
     * @param dx int distance to move in the X direction
     * @param dy int distance to move in the Y direction
     */
    abstract void move(int dx, int dy);

  }

  /**
   * MyShape subclass for drawing rectangles. A MyRectangle shape is defined by
   *  it's top left point, width, and height.
   */
  class MyRectangle extends MyShape{
    int w = 0;
    int h = 0;
    int leftX;
    int topY;

    /**
     * Constructor.
     */
    MyRectangle(Color color, boolean filled,
                int x1, int y1, int x2, int y2) {
      super(color, filled, x1, y1, x2, y2);
      leftX = Math.min(x1,x2); // determines which coordinate is closer to (0,0)
      topY = Math.min(y1,y2);  // determines which coordinate is closer to (0,0)
    }

    /**
     * Creates a new Rectangle2D, solid or outline only, object and paints
     *  it to the screen.
     */
    @Override
    public void draw(Graphics2D g2) {
      g2.setColor(color);
      if(filled){ // fill creates a solid shape
        g2.fill(new Rectangle2D.Double(leftX, topY, w, h));
      } else {  // draw is just the outline of the shape
        g2.draw(new Rectangle2D.Double(leftX, topY, w, h));
      }
    }

    /**
     * Update the upper left corner coordinates and calculate the new height
     * and width.
     */
    @Override
    public void update(int newX, int newY) {
      w = Math.abs(x2 - newX);   // distance between two x coordinates
      h = Math.abs(y2 - newY);   // distance between two y coordinates
      leftX = Math.min(newX,x2); // which x is closer to (0,0)
      topY = Math.min(newY,y2);  // which y is closer to (0,0)
    }

    /**
     * Tests if a point is either on or between the borders of a rectangle if
     *  solid. Otherwise only if it is on the border lines.
     */
    @Override
    public boolean contains(int px, int py) {
      boolean hit;
      if(filled){ // Is the point between the top or bottom or left or right?
        hit = (px >= x1 && px <= x1 + w) && (py >= y1 && py <= y1 + h);
      } else { // Is the point only on one of the 4 borders?
        hit = (px == x1 || px == x1 + w) && (py >= y1 && py <= y1 + h)
            || (py == y1 || py == y1 + h) && (px >= x1 && px <= x1 + w);
      }
      return hit;
    }

    /**
     * Adjusts all the coordinates of the rectangle by delta x and delta y.
     */
    @Override
    public void move(int dx, int dy) {
      leftX += dx;
      topY += dy;
      x2 += dx;
      y2 += dy;
      x1 += dx;
      y1 += dy;
    }
  }

  /**
   * MyShape subclass for drawing ellipses. A MyEllipse shape is defined by
   *  the top left point, width, and height of it's bounding rectangle.
   */
  class MyEllipse extends MyShape{
    double w = 0;
    double h = 0;
    int leftX;
    int topY;

    /**
     * Constructor.
     */
    MyEllipse(Color color, boolean filled,
              int x1, int y1, int x2, int y2) {
      super(color, filled, x1, y1, x2, y2);
      leftX = Math.min(x1,x2); // determines which coordinate is closer to (0,0)
      topY = Math.min(y1,y2);  // determines which coordinate is closer to (0,0)
    }

    /**
     * Creates a new Ellipse2D, solid or outline only, object and paints
     *  it to the screen.
     */
    @Override
    public void draw(Graphics2D g2) {
      g2.setColor(color);
      if(filled){  // fill creates a solid shape
        g2.fill(new Ellipse2D.Double(leftX, topY, w, h));
      } else {  // draw is just the outline of the shape
        g2.draw(new Ellipse2D.Double(leftX, topY, w, h));
      }
    }

    /**
     * Update the upper left corner coordinates and calculate the new height
     *  and width.
     */
    @Override
    public void update(int newX, int newY) {
      w = Math.abs(x2 - newX);   // distance between two x coordinates
      h = Math.abs(y2 - newY);   // distance between two y coordinates
      leftX = Math.min(newX,x2); // which x is closer to (0,0)
      topY = Math.min(newY,y2);  // which y is closer to (0,0)
    }

    /**
     * Tests if the given points are within the bounds of the ellipse if filled
     *  or just on the border.
     */
    @Override
    public boolean contains(int px, int py){
      boolean hit;

      // normalize the point coordinates
      Double point = (Math.pow((px - x1) /w - .5,2) +
          Math.pow((py - y1) /h - .5,2));
      if (filled){ // if the point is within the normalize radius
        hit = (int)Math.round(point*100) <= 25;
      } else { // if the point is on the normalize radius
        hit = (int)Math.round(point*100) == 25;
      }
      return hit;
    }

    /**
     * Adjusts all the coordinates of the rectangle by delta x and delta y.
     */
    @Override
    public void move(int dx, int dy) {
      leftX += dx;
      topY += dy;
      x2 += dx;
      y2 += dy;
      x1 += dx;
      y1 += dy;
    }
  }

  /**
   * MyShape subclass for drawing lines. A MyLine shape is defined by
   *  it's start and end points.
   */
  class MyLine extends MyShape{

    /**
     * Constructor. x1,y1 are the start coordinates and x2,y2 are the end
     *  coordinates
     */
    MyLine(Color color,
           int x1, int y1, int x2, int y2) {
      super(color, false, x1, y1, x2, y2);
    }

    /**
     * Draws a Line2D to the screen.
     */
    @Override
    public void draw(Graphics2D g2) {
      g2.setColor(color);
      g2.draw(new Line2D.Double(x1,y1,x2,y2));
    }

    /**
     * Changes the end point of the line.
     */
    @Override
    public void update(int x2, int y2) {
      this.x2 = x2;
      this.y2 = y2;
    }

    /**
     * Tests if the given coordinates are within 1 point of the line.
     */
    @Override
    public boolean contains(int px, int py){
      return Line2D.ptSegDistSq(x1, y1, x2, y2, px, py) <= 1;
    }

    /**
     * Adjusts the start and end coordinates of the rectangle by delta x
     *  and delta y.
     */
    @Override
    public void move(int x, int y) {
      x1 += x;
      y1 += y;
      x2 += x;
      y2 += y;
    }
  }
}