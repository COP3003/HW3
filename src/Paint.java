/*
 * FileName: Paint.java
 * Author: 5153
 * Created: 13 November, 2017
 * Last Modified: 17 November, 2017
 *
 * Included class:
 * <li>
 *   <ol>PaintShape</ol>
 *   <ol>Handler</ol>
 * </li>
 *
 * Paint is a simple paint program. It can draw three kinds of geometric shapes:
 *  rectangles, ovals, and edge in four colors: red, green, blue, and black.
 *  For rectangle and oval, they can be either filled or unfilled. For edge the
 *  filled checkbox does nothing. The drawn shapes can be removed one by one by
 *  clicking on "Undo" or completely by clicking "Clear". Neither action can be
 *  undone.
 *
 * Shapes can be selected and moved by right-dragging the mouse. Filled shapes
 *  can be selected by clicking within the shape. Unfilled shapes can only be
 *  selected when clicking on the border.
 *
 * Pressing the middle mouse button selects the last shape created and it can
 *  be resized.
 */

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Class Paint creates the application window and the all the components. It
 *  also manages user interactions with the Handler Class.
 *
 *  @author 5153
 */
public class Paint extends JFrame {
  private final Painter canvas;   // drawable surface and all the drawn shapes
  private final JButton undo;
  private final JButton clear;

  // dropdown selector boxes
  private final JComboBox<String> selectColor;
  private final JComboBox<PaintShape> selectShape;

  private final JCheckBox filled;

  private final JLabel mousePosition; // shows X,Y coordinates of the pointer

  private static final String[] colorNames = {"red","green","blue","black"};
  private static final List<Color> colors = new ArrayList<>(
            Arrays.asList(Color.red, Color.green, Color.blue, Color.black));

  /**
   * Enum of the shape types the user can select.
   */
  public enum PaintShape {
    RECTANGLE("rectangle"),
    OVAL("oval"),
    EDGE("edge");

    private final String name;
    PaintShape(String name){
      this.name = name;
    }

    // Only returns the String value of a specific shape.
    @Override
    public String toString(){
      return name;
    }
  }

  /**
   * default constructor
   */
  private Paint(){
    super("Paint");

    setLayout(new BorderLayout());
    setResizable(false);  // lock the window to 600x600

    mousePosition = new JLabel("(X,Y)"); // displays pointer coordinates

    undo = new JButton("Undo");
    clear = new JButton("Clear");

    // drop lists of selectable colors and shapes
    selectColor = new JComboBox<>(colorNames);
    selectShape = new JComboBox<>(PaintShape.values());

    filled = new JCheckBox("Filled");
    filled.setHorizontalTextPosition(SwingConstants.LEFT); // checkbox on right

    // create the paint surface with the default color and shape
    canvas = new Painter(colors.get(0), PaintShape.values()[0]);

    // Buttons, Lists, and Checkboxes Oh my!
    final JPanel controlPanel = new JPanel();
    controlPanel.add(undo);
    controlPanel.add(clear);
    controlPanel.add(selectColor);
    controlPanel.add(selectShape);
    controlPanel.add(filled);

    add(controlPanel, BorderLayout.NORTH);
    add(canvas, BorderLayout.CENTER);
    add(mousePosition, BorderLayout.SOUTH);

    // Object that defines how the application responds to different user input
    Handler handler = new Handler();

    // bind each component to specific user inputs
    undo.addActionListener(handler);
    clear.addActionListener(handler);
    selectColor.addItemListener(handler);
    selectShape.addItemListener(handler);
    filled.addItemListener(handler);
    canvas.addMouseListener(handler);
    canvas.addMouseMotionListener(handler);

    setSize(600,600); // Window dimensions
    setLocationRelativeTo(null);  // start in the center of the screen
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    setVisible(true);
  }

  /**
   * Class that defines how the application responds to different user input.
   * MouseAdapter is needed to respond to mouse movements, and mouse button
   *  activity.
   * ActionListener is for the undo and clear buttons.
   * ItemListener responds to changes in the color, shape of filled components.
   */
  private class Handler extends MouseAdapter implements ActionListener,
                                                        ItemListener{
    Point lastPos = new Point(); // mouse position from the last event

    // Responds to the undo and clear buttons and calls the respective methods
    //  in the Painter object. Then redraws the screen.
    @Override
    public void actionPerformed(ActionEvent e){
      if(e.getSource()==undo){
        canvas.undo();
        repaint();
      }else if(e.getSource()==clear) {
        canvas.clear();
        repaint();
      }
    }

    // Responds to changes in the color, shape of filled components by setting
    //  the corresponding variables.
    @Override
    public void itemStateChanged(ItemEvent e)
    {
      // process filled checkbox events
      if ( e.getSource() == filled )
      {
        canvas.filled = filled.isSelected();
      }

      // determine if a drop list selector was changed
      if ( e.getStateChange() == ItemEvent.SELECTED )
      {
        // If event source is the colors selector, set the Painter object color.
        if ( e.getSource() == selectColor)
        {
          canvas.fgColor=(colors.get(selectColor.getSelectedIndex()));
        }

        // Or if event source is the shapes selector, set the Painter object
        //  active shape.
        else if ( e.getSource() == selectShape)
        {
          canvas.stencil = (PaintShape.values()
              [selectShape.getSelectedIndex()]);
        }
      }

    }

    /**
     * When the right mouse button is pressed create a new shape object with the
     *  top left corner set to the mouse's position.
     * When the left mouse button is pressed this checks if there is a shape
     *  under the mouse pointer. If so that shape can be moved by dragging the
     *  mouse.
     */
    @Override
    public void mousePressed(MouseEvent e)
    {
      if (SwingUtilities.isRightMouseButton(e)){
        canvas.selected = canvas.layers.getShape(e.getX(), e.getY());
        if (canvas.selected != null) {
          lastPos = e.getPoint().getLocation();  // save the mouse position
        }
      } else if (SwingUtilities.isLeftMouseButton(e)) {
        canvas.addLayer(e.getX(), e.getY());  // creates a new MyShape object
      }
    }

    /**
     * When the right mouse button is release if a shape was being moved it is
     *  deselected and the previous mouse position is cleared.
     * When the left mouse button is release the last created MyShape is updated
     *  to the current mouse position.
     */
    @Override
    public void mouseReleased(MouseEvent e)
    {
      if (SwingUtilities.isRightMouseButton(e)){
        canvas.selected = null;
        lastPos.x = 0;
        lastPos.y = 0;
      } else {
        canvas.layers.peekLast().update(e.getX(), e.getY());
      }
    }


    // Gets the mouse position and sets it to mousePosition label.
    @Override
    public void mouseMoved(MouseEvent e)
    {
      mousePosition.setText(String.format("(%d,%d)",e.getX(),e.getY()));
    }

    /**
     * The right mouse button drag moves a selected MyShape object with the
     *  position of the mouse cursor.
     * The left mouse button drag sizes the last created shape until it is
     *  released.
     * An unintended but nice feature here is any mouse button other than right
     *  or left will select the last shape and allow it to be resized.
     * The mouse position is updated in the mousePosition label.
     */
    @Override
    public void mouseDragged(MouseEvent e)
    {
      //
      if (SwingUtilities.isRightMouseButton(e)){
        if (canvas.selected != null){
          canvas.selected.move(e.getX() - lastPos.x,e.getY() - lastPos.y);
          lastPos.x = e.getX();
          lastPos.y = e.getY();
        }
      } else {
        // sets the size, width, and position of the last MyShape created
        canvas.layers.peekLast().update(e.getX(), e.getY());
      }
      // sets mousePosition to current mouse position
      mousePosition.setText(String.format("(%d,%d)",e.getX(),e.getY()));

      repaint();  // redraw the screen
    }

    // Changes the mousePosition label to indicate the pointer is no longer over
    //  the Painter surface.
    @Override
    public void mouseExited(MouseEvent e){
      mousePosition.setText("Mouse is out");
    }

    // Superfluous method to update mousePosition as soon as the pointer is
    //  above the Painter surface.
    @Override
    public void mouseEntered(MouseEvent e){
      mousePosition.setText(String.format("(%d,%d)",e.getX(),e.getY()));
    }

  }

  // My main main.
  public static void main(String[] args) {
    new Paint();
  }
}
